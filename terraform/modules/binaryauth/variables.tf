variable attestor-name {
  type        = string
  description = "Name of the attestor"
}

variable keyring-id {
  type        = string
  description = "Keyring ID for attestor keys"
}

